# https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/20.0.0
module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster"
  version                    = "20.0.0"
  project_id                 = var.project_id
  name                       = var.gke_name
  description                = "test gke/k8s function"
  release_channel            = var.gke_release_channel
  regional                   = var.gke_regional
  region                     = var.vpc_gke_region
  zones                      = var.gke_zones
  logging_service            = "none"
  logging_enabled_components = [] # ["SYSTEM_COMPONENTS", "WORKLOADS"]
  network                    = module.network.network_name
  subnetwork                 = var.vpc_gke_subnets.node.subnet_name
  ip_range_pods              = var.vpc_gke_secondary_ranges.node.k8s-pod.range_name
  ip_range_services          = var.vpc_gke_secondary_ranges.node.k8s-service.range_name
  enable_private_nodes       = true
  master_ipv4_cidr_block     = var.gke_master_ipv4_cidr_block
  default_max_pods_per_node  = var.gke_default_max_pods_per_node
  remove_default_node_pool   = true
  gce_pd_csi_driver          = true
  horizontal_pod_autoscaling = true
  http_load_balancing        = true
  identity_namespace         = "enabled"
  maintenance_end_time       = var.gke_maintenance.end_time
  maintenance_recurrence     = var.gke_maintenance.recurrence
  maintenance_start_time     = var.gke_maintenance.start_time
  node_pools                 = var.gke_node_pools
  node_pools_labels          = var.gke_node_pools_labels
  node_pools_taints          = var.gke_node_pools_taints
  master_authorized_networks = var.gke_master_authorized_networks

  depends_on = [
    module.network
  ]

  /**********
  * unused *
  **********/
  kubernetes_version          = var.gke_kubernetes_version
  create_service_account      = var.gke_create_service_account
  grant_registry_access       = false
  enable_private_endpoint     = false
  network_policy              = false
  cluster_resource_labels     = {}
  enable_binary_authorization = false

  add_master_webhook_firewall_rules = var.gke_add_master_webhook_firewall_rules
  firewall_inbound_ports            = var.gke_firewall_inbound_ports
  node_pools_oauth_scopes           = var.gke_node_pools_oauth_scopes

  node_pools_metadata = var.gke_node_pools_metadata
  node_pools_tags     = var.gke_node_pools_tags
}
