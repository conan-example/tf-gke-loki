output "get_gke_credentials" {
  description = "Gcloud get-credentials command"
  # if you want to access it privately: --internal-ip
  value = format("gcloud container clusters get-credentials --project %s %s %s %s", var.project_id, var.gke_regional ? "--region" : "--zone", module.gke.location, module.gke.name)
}


output "gke_name" {
  description = "The name of the GKE"
  value       = module.gke.name
}
