module "cloud-router" {
  source  = "terraform-google-modules/cloud-router/google"
  version = "1.3.0"


  project = var.project_id
  name    = "cloud-router-gke"
  network = module.network.network_name
  region  = var.vpc_gke_region

  # https://registry.terraform.io/modules/terraform-google-modules/cloud-router/google/1.3.0
  # https://github.com/terraform-google-modules/terraform-google-cloud-router/blob/master/nat.tf
  nats = [
    {
      min_ports_per_vm = 1024
      name = "gke-cloud-nat"
    }
  ]
}
