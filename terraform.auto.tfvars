# project_id       = 
vpc_gke_region   = "asia-east1" 


vpc_gke_subnets = {
  node = {
    subnet_name           = "node"
    subnet_ip             = "10.1.144.0/23"
    subnet_private_access = true
  },

  lb-proxy = {
    subnet_name           = "lb-proxy"
    subnet_ip             = "10.1.152.0/21"
    subnet_private_access = false
  }
}
vpc_gke_secondary_ranges = {
  node = {
    k8s-pod = {
      range_name    = "k8s-pod"
      ip_cidr_range = "10.1.0.0/17"
    },
    k8s-service = {
      range_name    = "k8s-service"
      ip_cidr_range = "10.1.128.0/20"
    }

  }
}

/*******
 * GKE *
 *******/
gke_name                       = "loki-test" 
gke_regional                   = false
gke_create_service_account     = false
gke_zones                      = ["asia-east1-b", "asia-east1-a", "asia-east1-c"]
gke_release_channel            = "STABLE"
gke_master_ipv4_cidr_block     = "10.1.147.128/28"
gke_default_max_pods_per_node  = 64

gke_maintenance = {
  end_time   = "2006-01-02T22:00:00Z"
  recurrence = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU"
  start_time = "2006-01-02T18:00:00Z"
}
gke_add_master_webhook_firewall_rules = true
gke_node_pools = [{
  name           = "default-workload" # remember to update the relative resource like node_pools_taints if you modify the name of this node pool
  auto_repair    = true
  autoscaling    = true
  auto_upgrade   = true
  disk_size_gb   = 50
  disk_type      = "pd-standard"
  image_type     = "COS_CONTAINERD"
  machine_type   = "e2-standard-2" #"e2-custom-2-16384", "e2-custom-4-32768" "e2-medium" gcloud compute machine-types list --filter=e2
  min_count      = 1               # used by autoscaling
  max_count      = 4               # used by autoscaling
  node_count     = 1               # set the node count it autoscaling is false
  node_locations = "asia-east1-b"  # set it if you dont want to follow the default setting of cluster, comma to separate the zones, for example: "asia-east1-b,asia-east1-a"
  preemptible    = false
  spot           = true
  #   tags            = []   # instance tag
  # version = "1.21.10-gke.2000" # set it if you want ot override the default cluster setting, you can also comment this line to let it follow master's version
  }, {
  name           = "loki-ingester" # remember to update the relative resource like node_pools_taints if you modify the name of this node pool
  auto_repair    = true
  autoscaling    = true
  auto_upgrade   = true
  disk_size_gb   = 50
  disk_type      = "pd-standard"
  image_type     = "COS_CONTAINERD"
  machine_type   = "e2-standard-2"                              # "e2-custom-2-16384", "e2-medium", "e2-small" gcloud compute machine-types list --filter=e2
  min_count      = 1                                        # used by autoscaling
  max_count      = 2                                        # used by autoscaling
  node_count     = 1                                        # set the node count it autoscaling is false
  node_locations = "asia-east1-b" # set it if you dont want to follow the default setting of cluster, comma to separate the zones, for example: "asia-east1-b,asia-east1-a"
  preemptible    = false
  spot           = true
  #   tags            = []   # instance tag
  # version = "1.21.10-gke.2000" # set it if you want ot override the default cluster setting, you can also comment this line to let it follow master's version
  }
  , {
  name           = "loki-querier" # remember to update the relative resource like node_pools_taints if you modify the name of this node pool
  auto_repair    = true
  autoscaling    = true
  auto_upgrade   = true
  disk_size_gb   = 50
  disk_type      = "pd-standard"
  image_type     = "COS_CONTAINERD"
  machine_type   = "e2-custom-4-32768"                               #"e2-medium", "e2-small" gcloud compute machine-types list --filter=e2
  min_count      = 0                                        # used by autoscaling
  max_count      = 1                                        # used by autoscaling
  node_count     = 1                                        # set the node count it autoscaling is false
  node_locations = "asia-east1-b" # set it if you dont want to follow the default setting of cluster, comma to separate the zones, for example: "asia-east1-b,asia-east1-a"
  preemptible    = false
  spot           = true
  #   tags            = []   # instance tag
  # version = "1.21.10-gke.2000" # set it if you want ot override the default cluster setting, you can also comment this line to let it follow master's version
  }
]
gke_node_pools_labels = {
  all = {}

  loki-ingester = {
    node-purpose = "loki-ingester"
  }
  loki-querier = {
    node-purpose = "loki-querier"
  }

}

gke_node_pools_taints = {
  all = []

  loki-ingester = [
    {
      key    = "node-purpose-ne"
      value  = "loki-ingester"
      effect = "NO_EXECUTE" # NO_SCHEDULE, PREFER_NO_SCHEDULE, NO_EXECUTE, https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#effect
    },
    {
      key    = "node-purpose-ns"
      value  = "loki-ingester"
      effect = "NO_SCHEDULE" # NO_SCHEDULE, PREFER_NO_SCHEDULE, NO_EXECUTE, https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#effect
    }
  ]
  loki-querier = [
    {
      key    = "node-purpose-ne"
      value  = "loki-querier"
      effect = "NO_EXECUTE" # NO_SCHEDULE, PREFER_NO_SCHEDULE, NO_EXECUTE, https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#effect
    },
    {
      key    = "node-purpose-ns"
      value  = "loki-querier"
      effect = "NO_SCHEDULE" # NO_SCHEDULE, PREFER_NO_SCHEDULE, NO_EXECUTE, https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#effect
    }
  ]
}

gke_master_authorized_networks = [
  {
    display_name = "any-ip"
    cidr_block   = "0.0.0.0/0"
  }
]

