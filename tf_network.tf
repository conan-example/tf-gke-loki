module "network" {
  # https://registry.terraform.io/modules/terraform-google-modules/network/google/5.0.0
  source       = "terraform-google-modules/network/google"
  version      = "5.0.0"
  project_id   = var.project_id
  network_name = "gke-network"

  subnets          = local.vpc_subnets
  secondary_ranges = local.vpc_secondary_ranges

  description = "gke vpc"

  # unused currently
  delete_default_internet_gateway_routes = false
  shared_vpc_host                        = false
  routes                                 = []
  firewall_rules                         = []
  auto_create_subnetworks                = false
  routing_mode                           = "GLOBAL"
  mtu                                    = 0
}
