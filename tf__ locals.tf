locals {
  vpc_subnets = [
    for key, value in var.vpc_gke_subnets :
    {
      subnet_name           = value.subnet_name
      subnet_ip             = value.subnet_ip
      subnet_region         = var.vpc_gke_region
      subnet_private_access = value.subnet_private_access
    }
  ]

  vpc_secondary_ranges = {
    for key, value in var.vpc_gke_secondary_ranges : key =>
    [
      for secondary_key, secondary_value in value :
      {
        range_name    = secondary_value.range_name
        ip_cidr_range = secondary_value.ip_cidr_range
      }

    ]
  }
}
